<?php $id="top"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>


<ul class="c-list1">

<li><a href="/page/005.php">
コーディングフロー<br>
Coding flow
</a></li>

<li><a href="/page/001.php">
Font Awesomeは指示があるまで使わない<br>
Don't use Font Awesome.
</a></li>

<li><a href="/page/002.php">
PCでのフォントサイズルール<br>
PC font-size rule.
</a></li>

<li><a href="/page/003.php">
スマホでのサイズルール。<br>
PCの半分サイズにする。<br>
SP font-size, padding, margin and img-size rule<br>
Make it half the size of PC.
</a></li>


<li><a href="/page/004.php">
リキッドレイアウトでコーディングしない。<br>
Do not code in liquid layout.
</a></li>

<li><a href="/page/006.php">
レスポンシブCSSの記述<br>
CSS Design Technique for Responsive
</a></li>

<li><a href="/page/007.php">
区切り線を入れよう<br>
Let's put a separator line.
</a></li>


<li><a href="/page/008.php">
リンクの範囲<br>
Link area
</a></li>

<li><a href="/page/009.php">
カード型レイアウト<br>
Card type layout
</a></li>


<li><a href="/page/010.php">
&lt;img&gt;要素のサイズについて＆レスポンシブデザインの時の指定方法<br>
About the size of the &lt;img&gt; element & How to responsive web design
</a></li>


<li><a href="/page/011.php">
レスポンシブでの画像サイズのコツ<br>
Responsive image size tips
</a></li>

<li><a href="/page/012.php">
mixin clearfix　を使いましょう。<br>
Let's use mixin clearfix.
</a></li>


<li><a href="/page/013.php">
リンクにするかどうか<br>
Whether to link
</a></li>

<li><a href="/page/014.php">
altの中身を入れないで下さい<br>
Please do not put the contents of "alt".
</a></li>

<li><a href="/page/015.php">
"flex-basis"は使わない<br>
Don't use "flex-basis"
</a></li>



<?php /*
<li><a href="">FLOCSSとは</a></li>
<li><a href="">BEMとは</a></li>
<li><a href="">モジュールコーディングとは</a></li>
<li><a href="">コンポーネント化する</a></li>
<li><a href="">コンポーネントは使いまわす</a></li>
<li><a href="">CodePenにコンポーネントを登録する</a></li>
<li><a href="">BEMを使う</a></li>
<li><a href="">IDは使わない</a></li>
<li><a href="">区切りを入れる。長くしない</a></li>
<li><a href="">scssネストを深くしない</a></li>
<li><a href="">BEMネストを深くしない</a></li>
<li><a href="">.scssを分ける</a></li>
<li><a href="">分担して作成する</a></li>
<li><a href="">まずはコンポーネントから作成する</a></li>
*/ ?>

</ul>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>