<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
&lt;img&gt;要素のサイズについて＆レスポンシブデザインの時の指定方法<br>
About the size of the &lt;img&gt; element & How to responsive web design

</div>


<?php //===================================== ?>



<div class="c-text1">

<p>
&lt;img要素&gt;には"width" "height" "alt"の指定必須です。<br>
It's mandatory to specify "width" "height" "alt" for &lt;img&gt; element.
</p>

<p>
フルードイメージにする場合はwidthとheightを入れなくても良いです。<br>
It is not necessary to insert width and height when making a fluid image.
</p>

<p>
※フルードイメージ：画像のサイズをブラウザのウィンドウサイズに合わせて表示する方法<br>
* Fluid image: How to display the image size according to the browser window size
</p>

<p>
"width" "height"を指定する理由<br>
Why "width" "height" is specified
</p>

<p>
サイズを指定するとブラウザの読み込みのスピードが速くなるから<br>
Specifying the size will speed up the browser's loading speed.
</p>

<p>
何らかのトラブルで画像が表示されない時、サイズを指定しておけばその分のスペースが確保されるためレイアウトが崩れるのを防ぐことが可能です。<br>
When images are not displayed due to some troubles,<br>
if you specify the size, <br>
it is possible to prevent the layout from collapsing<br>
because space is secured for that.
</p>


<?php getimg("010_01.png"); ?>
<div class="c-text1">

<div class="c-title2">(1)</div>
<p>画像のwidth属性とheight属性が空です。<br>
	The width and height attributes of the image are empty.
</p>

<p>
問題ないように見えます<br>
It does not seem to be a problem.
</p>
</div>


<?php getimg("010_02.png"); ?>
<div class="c-text1">

<div class="c-title2">(2)</div>
<p>スマホで表示すると画像がぼやけます。高さが小数点になっているからです。<br>
Images are blurred when displayed with smartphones.<br>
That is because the height is incomplete.</p>

<div class="c-title2">(3)</div>
<p>
実機で見た時にこのように見えます。<br>
It looks like this when looking at the real machine.
</p>
</div>


<?php getimg("010_03.png"); ?>
<div class="c-text1">

<div class="c-title2">(4)</div>
<p>スマホのときは、画像のサイズをちょうど半分になるように指定します。<br>
For SP, specify the size of the image to be exactly half.</p>
</div>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>