<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
altの中身を入れないで下さい<br>
Please do not put the contents of "alt".
</div>


<?php //===================================== ?>


<?php getimg("014_01.png"); ?>
<div class="c-text1">
<p>altの中身が入っています。<br>
高度な日本語が要求されます。</p>
<p>It contains contents of alt.<br>
Advanced Japanese is required.</p>
</div>


<div class="c-text1">
<p>altの中身を入れないで下さい。<br>
日本語のチェック作業が行われます。<br>
大体の場合間違っています。<br>
そして、日本人も間違えます。
</p>
<p>Please do not put the contents of alt.
Japanese checking work will occur.<br>
In most cases it is wrong.<br>
And Japanese people make mistakes.</p>
</div>


<div class="c-text1">
<p>alt="" としてください。<br>
空欄です<br>
間違うくらいなら入力しないで下さい。
</p>
<p>Please set alt="",<br>
It is blank.<br>
Please do not enter it if it makes a mistake.</p>
</div>




<?php getimg("014_02.png"); ?>
<div class="c-text1">
<p>私たちは最後にaltを入力します。<br>
私はキーワード　alt=""　をまとめて検索しています。<br>
そして、私は一つ一つ注意深く日本語で入力しているのです。
</p>
<p>We will enter alt at the end.<br>
I am collectively searching with the keyword alt="".<br>
And I am carefully typing or copying in Japanese one by one.
</p>
</div>

<div class="c-text1">
<p>検索にヒットさせるために alt="" としてください。</p>
<p>To make search hit, so Please set alt=""</p>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>