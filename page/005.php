<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
コーディングフロー<br>
Coding flow
</div>


<?php getimg("005_01.png"); ?>
<div class="c-text1">
<p>プロジェクトの進め方です。<br>
まずはコンポーネントを作成してから各ページを作成します。<br>
その流れを説明します。</p>
<p>It is how to proceed with the project.<br>
First, I will create a component.<br>
Then create each page.<br>
I will explain the flow.</p>
</div>


<?php getimg("005_02.png"); ?>
<div class="c-text1">
<p>ヘッダー、フッターもコンポーネントだと考える。</p>
<p>I think that the header and footer are also components.</p>




<?php getimg("005_03.png"); ?>
<p>さらに分離して考えています。
このような”コンポーネント”だと考えます</p>
<p>I think about it further.<br>
This is a "component"</p>

<?php getimg("005_04.png"); ?>
<p>それらを組み合わせています。</p>
<p>They are combined.</p>
</div>


<?php getimg("005_05.png"); ?>
<div class="c-text1">
<p>タイトル部分をコンポーネントにする。<br>
作りやすいところから作る<br>
どこからでもいい。作る勇気が湧くところから。<br>
私はよくタイトルを最初に作ります。<br>
わかりやすいからです。<br>
</p>
<p>Make the title part a component.<br>
From easy to make.<br>
You can make it anywhere.<br>
I often make a title first.<br>
It is because it is easy to understand.</p>
</div>




<?php getimg("005_06.png"); ?>
<div class="c-text1">
<p>作らなくてはいけない部分の残り</p>
<p>The rest.</p>
</div>




<?php getimg("005_07.png"); ?>
<div class="c-text1">
<p>作りやすいところから作る</p>
<p>You can make it anywhere.</p>
</div>



<?php getimg("005_08.png"); ?>
<div class="c-text1">
<p>サイト上に1箇所しか無いが、これもコンポーネントとして扱います</p>
<p>There is only one place on these sites.<br>
However, this will also be treated as a component.</p>
</div>


<?php getimg("005_09.png"); ?>
<div class="c-text1">
<p>作らなくてはいけない部分の残り</p>
<p>The rest.</p>
</div>



<?php getimg("005_10.png"); ?>
<div class="c-text1">
<p>レイアウトが同じだが中身が違います。<br>
レイアウトだけ作ってしまいます。</p>
<p>The layout is the same but the contents are different.<br>
I make only the layout.</p>
<p>共通のフォントサイズを設定しておきます。</p>
<p>Set a common font size.</p>
</div>




<?php getimg("005_11.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)</div>
<p>上下余白を統一した枠を作った。<br>
これは今回のサイトだけのもの。<br>
このあたりは制作者によって違ってもいい。</p>
<p>I made a unified frame with upper and lower margins.<br>
This is only for this site.<br>
This will be totally different by developers</p>
<div class="c-title2">(2)</div>
<p>センタリングする枠。</p>
<p>The wrapper flame.</p>
<div class="c-title2">(3)</div>
<p>2通りが考えられます。<br>
これは担当者にお任せ。</p>
<p>There are two possible ways.<br>
Leave this to the developer.<br>
In your favorite way.</p>
<div class="c-title2">(4)</div>
<p>背景を設定するだけのクラス。</p>
<p>A class that only sets the background.</p>
</div>




<?php getimg("005_12.png"); ?>
<div class="c-text1">
<p>コンポーネントを組み合わせたコンポーネントを作ります。</p>
<p>Make components that combine components.</p>
</div>




<?php getimg("005_13.png"); ?>
<div class="c-text1">
<p>作らなくてはいけない部分の残り。<br>
もう共通部分がない。<br>
共通にできそうだが、そうでもないです。<br>
ここまでできたら、コンポーネント作成は完了です。</p>
<p>The rest.<br>
There is no common part anymore.<br>
It is likely to be common, but it is not so.<br>
Once done, the component creation is complete.</p>
</div>



<?php getimg("005_14.png"); ?>
<div class="c-text1">
<p>コンポ―ネント一覧の出来上がり。<br>
それぞれSCSSを分ける</p>
<p>Completion of component list.<br>
Separate each SCSS.</p>
</div>




<?php getimg("005_15.png"); ?>
<div class="c-text1">

<div class="c-title2">(1)</div>
<p>全てのデータを見渡して、</p>
<p>Look over all the data.</p>

<div class="c-title2">(2)</div>
<p>コンポーネントを分担して作成する。</p>
<p>Create components by sharing them.</p>

<div class="c-title2">(3)</div>
<p>最後に誰か一人がまとめる。</p>
<p>Finally someone gathers up.</p>

</div>


<?php getimg("005_16.png"); ?>
<div class="c-text1">
<p>これらが各ページ用のファイル</p>
<p>File for each page.</p>
</div>




<?php getimg("005_17.png"); ?>
<div class="c-text1">
<p>コンテンツ部分をくくります。<br>
クラス名の接頭辞は"p-"を使います。<br>
"p-"はこのように区別に使います。</p>
<p>Divide content parts.<br>
The class name prefix uses "p-".<br>
"p-" is used in this way for distinction.
</p>
</div>


<?php getimg("005_18.png"); ?>
<div class="c-text1">

<div class="c-title2">(1)</div>
<p>コーディングしやすいようにわける。</p>
<p>Divide it for easy coding.</p>

<div class="c-title2">(2)</div>
<p>この4ページはレイアウトが似ているので、"p-content1"を追加します。</p>
<p>Since the layout of these four pages are similar, "p-content 1" was added.</p>

</div>


<?php getimg("005_19.png"); ?>
<div class="c-text1">
<p>コンポーネントを組合わせます。<br>
余白を調整するだけです。</p>
<p>Combine components.<br>
Adjust the margins.</p>
</div>



<?php getimg("005_20.png"); ?>
<div class="c-text1">

<div class="c-title2">(1)</div>
<p>コンポーネント以外にもタグを追加してもよいです。<br>
"section"を追加しました。</p>
<p>You can also add tags besides components.<br>
"section" was added.</p>

<div class="c-title2">(2)</div>
<p>"p-news1"のなかでだけ、".c-title2"のフォントサイズを変更する例です。<br>
こうすればもともとのコンポーネントのスタイルを変えることありません（汚染がありません）</p>
<p>This is when you want to increase the font size of "c-title 2" only within "p-news1".<br>
Doing so will not pollute the style of the original component.</p>

<div class="c-title2">(3)</div>
<p>ここも同様です。</p>
<p>Here too.</p>

</div>



<?php getimg("005_21.png"); ?>
<div class="c-text1">
<p>"p-content1"は共通のスタイルを適用できます。</p>
<p>"p-content1" can apply a common style.</p>
</div>


<?php getimg("005_22.png"); ?>
<div class="c-text1">


<div class="c-title2">(1)</div>
<p>SCSSのネストを深くする必要はありません。</p>
<p>There is no need to deepen the nest at SCSS.</p>

<div class="c-title2">(2)</div>
<p>必要な部分だけスタイルを記入します。</p>
<p>Fill in the styles only for the necessary parts.</p>
</div>


<div class="c-text1">
<p>これがプロジェクトの進め方です。<br>
まずはコンポーネントを作成してから各ページを作成します。</p>
<p>It is how to proceed with the project.<br>
First, I will create a component.<br>
Then create each page.</p>
</div>

<?php /*

<?php getimg("005_22.png"); ?>
<div class="c-text1">
<p></p>
<p></p>
</div>



<?php getimg("005_22.png"); ?>
<div class="c-text1">
<p></p>
<p></p>
</div>



<?php getimg("005_22.png"); ?>
<div class="c-text1">
<p></p>
<p></p>
</div>

*/ ?>



<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>




