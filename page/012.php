<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
mixin clearfix　を使いましょう。<br>
Let's use mixin clearfix.
</div>


<?php //===================================== ?>


<?php getimg("012_01.png"); ?>
<div class="c-text1">
<p>clearするためのタグがあります。<br>
	これは無駄です。
それは何もコンテンツを持ちません。
HTML文法もよくないでしょう。<br>
</p>
<p>There is only html to "clear".<br>
This is useless.<br>
There is no content.<br>
HTML grammar is not good either.</p>
</div>

<?php getimg("012_02.png"); ?>
<div class="c-text1">
<p>mixinを用意してあります。</p>
<p>I have created a mixin for clear.</p>
</div>

<?php getimg("012_03.png"); ?>
<div class="c-text1">
<p>このように置き換えることが出来ます。<br>
ソースコードがシンプルになります。</p>
<p>You can replace it like this.<br>
The source code will be simple.</p>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>