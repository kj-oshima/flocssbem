<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
リンクの範囲<br>
Link area
</div>


<?php //===================================== ?>


<?php getimg("008_01.png"); ?>
<div class="c-text1">
<p>リンクの範囲が狭いです。</p>
<p>The link range is narrow.</p>
</div>

<?php getimg("008_03.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)</div>
<p>サイトのユーザーは、こういったリンクの範囲を想定しません。</p>
<p>Users of the site do not assume the scope of these links.</p>

<div class="c-title2">(2)</div>
<p>多くのユーザーはこの範囲を想定します。</p>
<p>Many users assume this range.</p>

</div>

<?php getimg("008_02.png"); ?>
<div class="c-text1">
<p>だからリンクの範囲はこうするべきです。</p>
<p>So widen the range of links like this.</p>
</div>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>