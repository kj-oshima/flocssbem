<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
カード型レイアウト<br>
Card type layout
</div>


<?php //===================================== ?>


<?php getimg("009_01.png"); ?>
<div class="c-text1">
<p>このコーディングは間違っています。<br>
無駄が多いです。</p>
<p>This coding is wrong.<br>
There is much waste.</p>
</div>

<?php getimg("009_02.png"); ?>
<div class="c-text1">
<p>ここのmarginを削除してみましょう。<br>
余白が発生します。<br>
この余白はブラウザごとに違います。<br>
それはコーディングするとき私たち苦しめます。<br>
レイアウトを狂わせるからです。</p>
<p>Let's delete margin here.<br>
Margins will be generated.<br>
This margin is different for each browser.<br>
It plagues us when coding.<br>
It is because I make the layout crazy.</p>
</div>

<?php getimg("009_03.png"); ?>
<div class="c-text1">
<p>htmlはもっとシンプルにできます。<br>
float:leftで詰めてしまいます。<br>
</p>
<p>html can be made more simple.<br>
"float:left" to eliminate the margin.</p>
</div>

<?php getimg("009_04.png"); ?>
<div class="c-text1">
<p>そして余白を調整します。</p>
<p>And adjust the margins.</p>
</div>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>