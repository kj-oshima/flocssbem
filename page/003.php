<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
スマホでのフォントサイズルール。<br>
PCの半分サイズにする。<br>
SP font-size rule.<br>
Make it half the size of PC.
</div>


<?php getimg("003_01.png"); ?>

<div class="c-text1">

<p>デザインデータが再現されています。<br>
良さそうに見えますが、これは間違いです。</p>

<p>Good.<br>
Design data is reproduced.<br>
However, it is wrong.</p>

</div>



<?php getimg("003_02.png"); ?>

<div class="c-text1">

<div class="c-title2">(1)</div>

<p>デザインデータは2倍のサイズで作られています。<br>
Ratinaディスプレイを考慮しています。<br>
そのため半分のサイズでコーディングします。</p>

<p>Design data is made twice the size.<br>
It considers Ratina display.<br>
Therefore, we have to code with half the size.</p>

<p>50%に縮小してみましょう。<br>
このページをスマホで表示した状態になります。<br>
デザインデータの作者はこの表示を意図しています。</p>

<p>Let’s reduce the scale to 50%.<br>
This is displayed on a smartphone.<br>
The author of this PSD assumes this display.</p>

</div>





<?php getimg("003_03.png"); ?>

<div class="c-text1">

<div class="c-title2">(2)</div>

<p>このように半分のサイズでコーディングします。</p>
<p>Coding with half the size like this.</p>

</div>




<?php getimg("003_04.png"); ?>

<div class="c-text1">

<p>paddingやmarginも半分のサイズにします。</p>
<p>Also padding and margin are half size.</p>

</div>


<?php getimg("003_05.png"); ?>

<div class="c-text1">

<p>アイコンも半分サイズです</p>
<p>Icon is also half size.</p>

</div>






<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>