<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
画像サイズのコツ<br>
Image size tips
</div>


<?php //===================================== ?>


<?php getimg("011_01.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)</div>
<p>文字サイズが大きい。</p>
<p>The font-size is large.</p>
<div class="c-title2">(2)</div>
<p>画像サイズが小さい。</p>
<p>The image size is small.</p>
</div>

<?php getimg("011_02.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)(2)</div>
<p>それぞれこのように修正しました。</p>
<p>Each of these was fixed.</p>
</div>

<?php getimg("011_03.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)</div>
<p>まずはiPhone6だけを考慮します。<br>
シェアが多いからです。</p>
<p>First of all, I will only consider "iPhone 6".<br>
It is because it has a lot of market share.</p>
<div class="c-title2">(2)</div>
<p>文字サイズが大きい。</p>
<p>The font-size is large.</p>
<div class="c-title2">(3)(4)</div>

<p>これをインストールしましょう。</p>
<p>Let's install it.</p>

<p>↓↓↓↓↓↓↓<br>
<a href="https://dev.classmethod.jp/tool/emmet-sublimetext2-1/" target="_blank">About Emmet 1</a><br>
<a href="https://dev.classmethod.jp/tool/sublime-text-2-emmet-coding-2/" target="_blank">About Emmet 1</a><br>
↑↑↑↑↑↑↑<br>
</p>

<p>画像にはサイズを入れましょう。</p>
&lt;img>タグの上にカーソルをあわせてCTL+Uを押します。
<p>Let's put a size in the image.<br>
Place the cursor on the &lt;img> tag and press CTRL + U.</p><br>





<video src="../assets/image/011_m01.mp4" autoplay autobuffer autoloop loop controls poster="/images/video.png"></video>
<div class="c-title2">(5)</div>
<p>この２つはサイズを半分にします。</p>
<p>These two halves the size.</p><br>

<p>「HalfImageSize」を使うと便利です。</p>
<p>It is convenient to use "HalfImageSize".</p>
<br><br>
↓↓↓↓↓↓↓<br>
<a href="http://orememo-v2.tumblr.com/post/77230013409/%E7%94%BB%E5%83%8F%E3%81%AEwidth-height%E3%81%AE%E5%80%A4%E3%82%92%E5%8D%8A%E5%88%86%E3%81%AB%E3%81%99%E3%82%8Bsublime-text%E3%81%AE%E3%83%97%E3%83%A9%E3%82%B0%E3%82%A4%E3%83%B3%E3%82%92%E6%9B%B8%E3%81%84%E3%81%9F%E3%82%88" target="_blank">HalfImageSize</a><br>
↑↑↑↑↑↑↑<br>
<br>
<br>
<video src="../assets/image/011_m02.mp4" autoplay autobuffer autoloop loop controls poster="/images/video.png"></video>

</div>

<?php getimg("011_04.png"); ?>
<div class="c-text1">
<div class="c-title2">(1)</div>
<p>デザイン上で、画像の幅が100%に満たないものは半分サイズにします</p>
<p>When design is less than 100% wide, we will make it half size.</p>
<div class="c-title2">(2)</div>
<p>幅が100%のものは、そのままのサイズで表示します</p>
<p>Those with a width of 100% are displayed as they are.</p>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>