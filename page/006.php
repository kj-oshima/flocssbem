<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
レスポンシブCSSの記述<br>
CSS Design Technique for Responsive
</div>


<?php getimg("006_01.png"); ?>


<div class="c-text1">

<div class="c-title2">(1)</div>

<p>この方法は正しいです。<br>
でも、もっといい方法を見つけました。</p>

<p>Is this correct.<br>
But I found a better way.</p>

<div class="c-title2">(2)</div>

<p>mixinを用意します。</p>
<p>make mixin.</p>

<div class="c-title2">(3)</div>

<p>こうします。<br>
コードがわかりやすくなります。<br>
変更もしやすい。</p>
<p>I write like this.<br>
It makes code easier to understand.<br>
It is easy to change.</p>



</div>


<?php getimg("006_02.png"); ?>


<div class="c-text1">

<p>要素ごとに細かく記述できます。</p>
<p>It can be described finely for each element.</p>

</div>













<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>