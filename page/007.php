<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
区切り線を入れよう<br>
Let's put a separator line.
</div>



<?php getimg("007_01.png"); ?>
<div class="c-text1">

<div class="c-title2">(1)</div>
<p>コードが長いです。<br>
もしも、このコードを1年後に変更する場合、把握が難しい。<br>
コードを変更するのはあなた自身かもしれないし、他人かもしれない。<br>
できるだけわかりやすくしよう。</p>
<p>The code is long.<br>
It is difficult to understand.<br>
Changing this code after one year makes it difficult to grasp.<br>
Changing the code may be done by yourself or someone else.<br>
Let's make it as easy as possible.</p>


<div class="c-title2">(2)</div>
<p>In addition, responsive CSS is written under this long code!<br>
The code will be longer!</p>
<p>さらに、この長いコードの下にレスポンシブCSSが書いてある！<br>
さらにコードが長くなる！</p>

</div>




<?php getimg("007_02.png"); ?>
<div class="c-text1">

<div class="c-title2">(3)</div>
<p>せめて区切り線を入れましょう。<br>
機能ごとに分けましょう。</p>
<p>Let's put a separator at least.<br>
Let's divide each item.</p>

<div class="c-title2">(4)</div>
<p>もしあなたがネスト内で区切り線を入れる場合、CSSにそれが出力されてしまいます。</p>
<p>If you put a separator line inside the nest, it will be output to the CSS.</p>

<p>
//<br>
これを使うとCSSに出力されません
</p>
<p>
//<br>
Using this will not be output to CSS
</p>


<div class="c-title2">(5)</div>
<p>ここにコメントを書くことをおすすめします。<br>
</p>
<p>I suggest you write comments here.</p>


</div>





<?php getimg("007_03.png"); ?>
<div class="c-text1">

<div class="c-title2">(6)</div>


<p>たとえコーディングするのが1ページでも、項目ごとにSCSSを分けましょう。<br>
もしこれらが項目として独立しているのなら各.scssに入れましょう</p>
<p>Even if you code one page, let's divide the SCSS for each item.<br>
If they are independent as items, let's put in each .scss</p>

<p>例　Example<br>
.c-mainttl →　2_component/_title.scss<br>
.c-text →　2_component/_other.scss<br>
.l-content →　1_layout/layout.scss<br>
</p>



</div>









<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>