<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
リキッドレイアウトでコーディングしない。<br>
Do not code in liquid layout.
</div>


<?php getimg("004_01.png"); ?>

<div class="c-text1">

<div class="c-title2">(1)</div>
<p>ウィンドウ幅が広い場合。良い。</p>
<p>When the window width is wide.<br>
It's good.</p>

<div class="c-title2">(2)</div>

<p>ウィンドウ幅を狭くすると、レイアウトが崩れます。<br>
これは、リキッドレイアウトでコーディングしているからです。<br>
特別なレイアウトではない限り、私たちはリキッドレイアウトでコーディングしません。</p>

<p>However, narrowing the window width is not good.<br>
That is because you are thinking in the liquid layout.<br>
Unless it is a special layout, we do not code in liquid layout.</p>


<div class="c-title2">(3)</div>
<p>ウィンドウ幅を狭くすると、レイアウトが崩れます。</p>
<p>The layout is broken.<br>

<div class="c-title2">(4)</div>
<p>レイアウトは崩れていないように見える。<br>
しかし、ウィンドウ幅をどんどん狭めると、いずれ崩れる</p>
<p>The layout does not look broken. <br>
However, narrowing the width of the window further will break.</p>

</div>



<?php getimg("004_02.png"); ?>

<div class="c-text1">

<div class="c-title2">(5)</div>

<p>デザインデータをブラウザ上に再現すればいい。<br>
そう考えてください。</p>
<p>You can reproduce the design data on the browser. <br>
Please think so.</p>


<div class="c-title2">(6)</div>

<p>ウィンドウ幅を狭くしてもレイアウトは変えません</p>
<p>Shrinking the width of the window does not change the layout.</p>

</div>





<?php getimg("004_03.png"); ?>

<div class="c-text1">

<div class="c-title2">(7)</div>

<p>max-widthではなく、widthが正しい。</p>
<p>Then, "max-width" is wrong.<br>
"width" is correct.</p>
</div>




<?php getimg("004_04.png"); ?>

<div class="c-text1">

<div class="c-title2">(8)</div>

<p>この部分はサイト幅を無視しています。<br>
ここはリキッドレイアウトにしましょう。</p>
<p>This part ignores the site width.
In that case it is a liquid layout.</p>
</div>



<?php getimg("004_05.png"); ?>

<div class="c-text1">

<div class="c-title2">(9)</div>

<p>リキッドレイアウトを前提としているから、コーディングに％が多用されています。<br>
％は便利です。
しかし多くの欠点があります。</p>
<p>In order to correspond to liquid layout, you are using “%”.<br>
Using “%” is useful.<br>
However, there are many disadvantages.</p>
</div>



<?php getimg("004_06.png"); ?>

<div class="c-text1">

<div class="c-title2">(10)</div>

<p>画像がぼやけていることがあります</p>
<p>The image may be blurry.</p>

<div class="c-title2">(11)</div>

<p>
多くの原因は"%"	によるものです。<br>
幅が固定じゃないからです。</p>
<p>Many reasons are due to "%". <br>
The layout changes depending on the width of the window.
It is the liquid layout that is causing the cause.</p>

</div>



<?php getimg("004_07.png"); ?>

<div class="c-text1">

<p>私たちはPCとSPのデータしか用意しません。<br>
リキッドレイアウトのデザインデータを作るのは難しいからです。<br>
コーディングも難しい。<br>
確認にも時間がかかる<br>
そのため、特別な場合以外はリキッドレイアウトでコーディングしません。<br>
PCとSPの表示しか再現しません。
</p>

<p>
We only prepare two kinds of data, PC and SP.<br>
It is very hard to create liquid layout data.<br>
It is hard to code.<br>
It is difficult to confirm.<br>
So we do not code in liquid layout except in special cases.<br>
Only PC and SP do not reproduce.
</p>

</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>