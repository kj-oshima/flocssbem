<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
Font Awesomeは指示があるまで使わない<br>
Don't use Font Awesome.
</div>


<?php getimg("001_01.png"); ?>


<div class="c-text1">

<div class="c-title2">(1)</div>

<p>Font awesomeが使われています。<br>
この方法は正しいです。<br>
ページの表示速度が速くなりますし、なにより便利です。</p>

<p>You are using a "Font awesome".<br>
Is this correct.<br>
The display speed of the page also becomes light.</p>

<div class="c-title2">(2)</div>

<p>でも私たちはデザインデータを再現したいです。<br>
私たちは似たアイコンを表示したいのではありません。<br>
基本的には"Font awesome"を使わないでください。<br>
"Font awesome"を使う場合は別途で指示します。</p>

<p>But we want to reproduce design data (.psd, .ai, .png).<br>
We do not want to display similar icons.<br>
Basically, you should not use "Font awesome".<br>
When using "Font awesome", we will order it separately.</p>

</div>















<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>