<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
リンクにするかどうか<br>
Whether to link
</div>


<?php //===================================== ?>


<?php getimg("013_01.png"); ?>
<div class="c-text1">
<p>これらはリンクです。<br>
ボタン風のデザインで、メールのアイコンがついていたらリンクです。
</p>
<p>These are links.<br>
It is a button style design, with a mail icon attached.　It is a link.</p>
</div>


<?php getimg("013_02.png"); ?>
<div class="c-text1">
<p>これもリンクです。<br>
ボタン風のデザインで、矢印のアイコンが付いていたらリンクです。</p>
<p>This is also a link.
It is a button style design, with an arrow icon attached.　It is a link.</p>
</div>


<?php getimg("013_03.png"); ?>
<div class="c-text1">
<p>デザインによりますが、リンクを範囲を全体にしてあげて下さい。<br>
ユーザーがクリックしやすい方法を考慮します。
</p>
<p>Depending on the design, please give the link the whole area.<br>
Consider how easy for users to click.</p>
</div>


<?php getimg("013_04.png"); ?>
<div class="c-text1">
<p>電話番号もリンクです。<br>
	PCの場合はリンクにしません。<br>
SPの場合に、telリンクにします。</p>
<p>The phone number is also a link.<br>
	For PC, it is not a link.<br>
For SP, use &lt;a href="tel:045-341-3600"&gt; as a link.</p>
</div>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>