<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="c-title1">
"flex-basis"は使わない<br>
Don't use "flex-basis"
</div>


<?php //===================================== ?>


<?php getimg("015_01.png"); ?>
<div class="c-text1">
<p>"display:flex"を使ってコンテンツを横並びにするとき、<br>
要素のサイズを”flex-basis”で指定するとInternet Explorer（IE）で崩れます。</p>
<p>When using "display: flex" to arrange contents horizontally,<br>
Specifying the element size as "flex-basis" will not display the correct display in Internet Explorer (IE).</p>
</div>

<div class="c-text1">
	<?php getimg("015_02.png"); ?>
	<p>要素のサイズはwidthで管理しましょう。</p>
	<p>Let's manage the element size with width.</p>
</div>
<div class="c-text1">	
	<p>*"flex-basis"は、"flex-grow, flex-shrink"をセットで使います。<br>
	"flex-grow、flex-shrink"を使用しない時は「width」を使用します。</p>
	<p>*"Flex-basis" uses "flex-grow, flex-shrink" as a set.<br>
	When "flex-grow, flex-shrink" is not used, use "width".</p>
</div>





<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>